const axios = window.axios;

// Get state data from JSON
export function getJSON() {
    console.log("In getJSON");
    return axios.get("../data/data.json").then(
        (response) => {
            return response.data.States;
        }
    );
};