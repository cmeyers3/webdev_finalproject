import { useEffect, useState } from 'react';
import { getAllComments } from '../../Common/Services/CommentService.js';
//import { getById, getAllStates } from '../../Services/StateService.js';

function Comments() {
    // Retrieve comments from database on page load
    const [comments, setComments] = useState([]);
    useEffect(() => {
        getAllComments().then((comments) => {
            setComments(comments);
        });
    }, []);

    return (
        comments.map(
            comment => ( 
                <div key={comment.id} id='Comments'>         
                    <h4>{comment.attributes.title}</h4>
                    <h6>Author: {comment.attributes.author}</h6>
                    <h5>{comment.attributes.body}</h5>
                </div>
            )
        )
    );
};

export default Comments;
