import React from "react";
import { Link, useParams, Redirect } from "react-router-dom";
import { isAuthorized } from '../../Common/Services/AuthService.js';

// You must make sure to check this protected route even when manually typing in url
// or you can easily bypass the auth
const UserGood = () => {
  const { firstName, lastName } = useParams();
    
  console.log(isAuthorized());
    
  if (isAuthorized()) {
    return (
        <div>
          <h1>
            {" "}
            User: {firstName} {lastName}{" "}
          </h1>
          <button>
            <Link to="/auth/logout">Logout.</Link>
          </button>
        </div>
    );
  }
  else {
      return (
          <Redirect to='/auth' />
      );
  }

};

export default UserGood;
