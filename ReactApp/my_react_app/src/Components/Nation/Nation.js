import React, { useEffect, useState } from 'react'
import { getById } from '../../Common/Services/NationService.js';

function Nation() {
    // Get US nation
    const [nation, setNation] = useState();
    useEffect(() => {
        getById("JxiX1GzkOI").then((nation) => {
            setNation(nation);
        });
    }, []);
    
    var nationData = (
        <div></div>
    );
    if (nation) {
        nationData = (
            <div>
                <h3><u>{nation.attributes.name}</u></h3>
                <h4>Population: {nation.attributes.population}</h4>     
                <h4>Case Count: {nation.attributes.caseCount}</h4>
                <h4>Vaccine Count: {nation.attributes.vaccineCount}</h4>
            </div>
        );
    }
    
    return (
        <div>
            {nationData}
        </div>
    );
}

export default Nation;
