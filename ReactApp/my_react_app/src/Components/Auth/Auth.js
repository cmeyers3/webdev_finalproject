import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { isAuthorized } from '../../Common/Services/AuthService.js';

const AuthModule = () => {
    
    if (isAuthorized()) {
        return (
            <Redirect to='/'/>
        );
    }
    
    return (
        <div>
            <Link to='/auth/register'><button>Register</button></Link>
            <br />
            <br />
            <Link to='/auth/login'><button>Login</button></Link>
        </div>
    );
};

export default AuthModule;