import React, {useEffect, useState} from 'react';
import { checkUser } from '../../Common/Services/AuthService';
import AuthLoginForm from './AuthLoginForm.js';
import ProtectedRoute from '../../Common/AppTools/ProtectedRoute.js';
import UserGood from '../User/UserGood.js';

const AuthLogin = () => {
    // Create empty username and password
    const [newUsername, setNewUsername] = useState("");
    const [newPassword, setNewPassword] = useState("");
    

    // Flags in state to watch for add/remove updates
    const [add, setAdd] = useState(false);
    const [flag, setFlag] = useState(false);
    
    const [userPath, setUserPath] = useState('');
    
    // useEffect that run when changes are made to the statevariable flags
    useEffect(() => {
        if (newUsername && newPassword && add) {
            console.log("New Username: " + newUsername);
            checkUser(newUsername, newPassword).then((userLogin) => {
                console.log(userLogin);
                if (userLogin) {
                    alert(`${userLogin.get("username")}, you successfully logged in!`);
                    
                    setUserPath('/user/' + userLogin.attributes.firstName + '/' + userLogin.attributes.lastName);
                    console.log('Here')
                    setFlag(true);
                }
                // TODO: Redirect user to main app
                setAdd(false);
                
            });
        }
    }, [newUsername, newPassword, add]);

    const onChangeHandler = (e) => {
        e.preventDefault();
        console.log('changed: ', e.target);
        const { name, value: newValue } = e.target;
        console.log(newValue);
        
        if (name === 'email') {
            setNewUsername(newValue)
            console.log("New Username: " + newUsername);
        }
        else if (name === 'password') {
            setNewPassword(newValue)
        }
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        console.log('submitted: ', e.target);
        // Update new user 
        setAdd(true);
    };

    if (flag) {
        return (
            <div>
                <ProtectedRoute
                    exact
                    path={userPath}
                    flag={true}
                    component={UserGood}
                    />
            </div>
        );  
    }
    else {
        return (
            <AuthLoginForm
                userName={newUsername}
                password={newPassword}
                onChange={onChangeHandler}
                onSubmit={onSubmitHandler} />
        );
    }
};

export default AuthLogin;







