import { Redirect } from 'react-router-dom';
import { logOut } from '../../Common/Services/AuthService.js';

const AuthLogout = () => {
    logOut(); 
    console.log("You logged out");
    return (
        <Redirect to='/auth' />
    );
};

export default AuthLogout;