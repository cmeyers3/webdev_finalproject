import React, {useEffect, useState} from 'react';
import { createUser } from '../../Common/Services/AuthService';
import AuthRegisterForm from './AuthRegisterForm.js';

const AuthRegister = () => {
    const [newUser, setNewUser] = useState({
        firstName: '',
        lastName: '',
        email: '',
        password: ''
    });

    // Flags in state to watch for add/remove updates
    const [add, setAdd] = useState(false);

    // useEffect that run when changes are made to the statevariable flags
    useEffect(() => {
        if (newUser && add) {
            createUser(newUser).then((userCreated) => {
                if (userCreated) {
                    alert(`${userCreated.get("firstName")}, you successfully registered!`)
                }
                // TODO: Redirect user to main app
                setAdd(false);
            });
        }
    }, [newUser, add]);

    const onChangeHandler = (e) => {
        e.preventDefault();
        console.log('changed: ', e.target);
        const { name, value: newValue } = e.target;
        console.log(newValue);
        
        setNewUser({
            ...newUser,
            [name]: newValue
        });
        
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();
        console.log('submitted: ', e.target);
        // Update new user 
        setAdd(true);
    };

    return (
        <div>
            <AuthRegisterForm
                user={newUser}
                onChange={onChangeHandler}
                onSubmit={onSubmitHandler} />        
        </div>
    );
};

export default AuthRegister;







