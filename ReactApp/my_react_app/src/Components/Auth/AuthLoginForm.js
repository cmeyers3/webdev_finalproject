import React from 'react';

const AuthLoginForm = ({ userName, password, onChange, onSubmit }) => {
    return (
        <div>
            <form onSubmit={onSubmit} autoComplete='off'>
                <div className='form-group'>
                    <label>Email</label>
                    <br />
                    <input  
                        type='email'     
                        className='form-control' 
                        id='email-input' 
                        value={userName}
                        onChange={onChange}
                        name='email'
                        required
                    />
                </div>
                <div className='form-group'>
                    <label>Password</label>
                    <br />
                    <input  
                        type='password'     
                        className='form-control' 
                        id='password-input' 
                        value={password}
                        onChange={onChange}
                        name='password'
                        min='0'
                        required
                    />
                </div>
                <div className='form-group'>
                    <button 
                        type='submit' 
                        className='btn btn-primary' 
                        onSubmit={onSubmit}
                    >Submit</button>
                </div>
            </form>
        </div>
    );
};

export default AuthLoginForm;
