import React, { useState } from "react";
import ReactTooltip from 'react-tooltip';
//import StateGrid from './State/StateGrid.js';
import SearchBox from './SearchBox/SearchBox.js';
import Nation from './Nation/Nation.js';
import USMap from './USMap/USMap.js';
//import CommentForm from './Comment/CommentForm.js';
import Comments from './Comment/Comment.js';
import Footer from './Footer/Footer.js';
import StatePage from './StatePage/StatePage.js';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Import Routing
//import AuthModule from './Auth/Auth.js';
//import AuthRegister from './Auth/AuthRegister.js';
//import AuthLogin from './Auth/AuthLogin.js';
//import AuthLogout from './Auth/AuthLogout.js';

//import UserGood from './User/UserGood.js'

            //<div id='StateGrid'>
            //    <Route path='/' exact component={StateGrid} />
            //</div>

const Components = () => {
    const [tooltipContent, setTooltipContent] = useState("");
    return (
        <Router>
            <div id='Nation'>
                <Route path='/' exact component={Nation} />
            </div>
            <div>
                <Route path='/' exact>
                    <USMap setTooltipContent={setTooltipContent} />
                    <ReactTooltip html={true} multiline={true}>{tooltipContent}</ReactTooltip>
                </Route>
            </div>

            <div>
                <Route path='/' exact component={SearchBox} />
            </div>
            <div id='StatePage'>
                <Route path='/state/:stateAbbrev' component={StatePage} />
            </div>
            <div>
                <Route path='/' exact component={Comments} />
            </div>
            <Footer />
        </Router>
    );
}

/* If add back auth: <Switch>
                <Route path='/auth' exact component={AuthModule} />
                <Route path='/auth/register' exact component={AuthRegister} />
                <Route path='/auth/login' component={AuthLogin} />
                <Route path='/auth/logout' component={AuthLogout} />
        
                <Route path='/user/:firstName/:lastName' exact component={UserGood} />
        
                <Redirect to='/' />
            </Switch>
*/

export default Components;

