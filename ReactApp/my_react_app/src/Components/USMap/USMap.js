import React, { useState, useEffect, memo } from "react";
import { useHistory } from "react-router-dom";
import { getAllStates, getStateByName } from '../../Common/Services/StateService.js';
import { geoCentroid } from "d3-geo";
import { scaleLinear } from "d3-scale";
import {
    ComposableMap,
    Geographies,
    Geography,
    Marker,
    Annotation
} from "react-simple-maps";
import allStates from "../../data/allstates.json";
const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";

// Offsets for annotations
const offsets = {
    VT: [50, -8], NH: [34, 2], MA: [30, -1], RI: [28, 2], CT: [35, 10],
    NJ: [34, 1], DE: [33, 0], MD: [47, 10], DC: [49, 21]
};


// getColor() - Retrieve [stateName, color] from a state object
function getColor(state) {
    // Create color scale
    const customScale = scaleLinear()
        .domain([31.6, 61])
        .range(["yellow", "green"])

    var percentVaccinated = state.attributes.percentVaccinated;
    return [state.attributes.name, customScale(percentVaccinated)];
}


// getColors() - Retrieve a list of [stateName, color] for each state object
function getColors() {
    return getAllStates().then((states) => {
        return states.map(getColor);
    });
}


// Create a service request that only calls once on load, to fetch all states -> local var state
const handleMouseEnter = (setTooltipContent, geo) => {
    const NAME = geo.properties.name

    // Get all states
    getStateByName(NAME).then((state) => {
        if (state.length === 0) {
            setTooltipContent(`${NAME}`);
        }
        else {
            var vaccineCount = state[0].attributes.vaccineCount;
            var percentVaccinated = state[0].attributes.percentVaccinated;
            setTooltipContent(`
                ${NAME} 
                <br />
                Vaccine Count: ${vaccineCount}
                <br />
                Percent Vaccinated: ${percentVaccinated}
            `);
        }
    })
}   


const handleMouseLeave = (setTooltipContent) => {
    setTooltipContent("");
}


const USMap = ({ setTooltipContent }) => {
    // Retrieve colors
    const [colors, setColors] = useState([]);
    useEffect(() => {
        getColors().then((newColors) => {
            setColors(newColors);
        });
    }, []);
    
    // Handle click
    const history = useHistory();
    function HandleClick(name) {
        getStateByName(name).then((state) => {
            history.push("/state/" + state[0].get('abbrev'));
        });  
    }

    // Return map
    return (
        <ComposableMap id="USMap" data-tip="" projection="geoAlbersUsa">
            <Geographies geography={geoUrl}>
                {({ geographies }) => (
                    <>
                        {geographies.map((geo, index) => {
                            // Retrieve color of current state
                            var color_object = colors.filter(x => x[0] === geo.properties.name)[0];
                            var color = "";
                            if (color_object) {
                                color = color_object[1]; 
                            }
                            return (
                                <Geography
                                    key={geo.rsmKey}
                                    geography={geo}
                                    fill={color}
                                    onMouseEnter={() => handleMouseEnter(setTooltipContent, geo)}
                                    onMouseLeave={() => handleMouseLeave(setTooltipContent)}
                                    onClick={() => HandleClick(geo.properties.name)}
                                    style={{
                                        hover: {
                                            fill: "gray",
                                            outline: "black"
                                        },
                                        pressed: {
                                            fill: "gray",
                                            outline: "none"
                                        }
                                    }}
                                />
                        )})}
                        {geographies.map(geo => {
                            const centroid = geoCentroid(geo);
                            const cur = allStates.find(s => s.val === geo.id);
                            return (
                                <g 
                                key={geo.rsmKey + "-name"}
                                >
                                    {cur &&
                                        centroid[0] > -160 &&
                                        centroid[0] < -67 &&
                                        (Object.keys(offsets).indexOf(cur.id) === -1 ? (
                                        <Marker coordinates={centroid}>
                                            <text y="2" fontSize={14} textAnchor="middle">
                                                {cur.id}
                                            </text>
                                        </Marker>
                                        ) : (
                                        <Annotation
                                            subject={centroid}
                                            dx={offsets[cur.id][0]}
                                            dy={offsets[cur.id][1]}
                                        >
                                            <text x={4} fontSize={14} alignmentBaseline="middle">
                                                {cur.id}
                                            </text>
                                        </Annotation>
                                    ))}
                                </g>
                            );
                        })}
                    </>
                )}
            </Geographies>
        </ComposableMap>
    );
};

export default memo(USMap);

