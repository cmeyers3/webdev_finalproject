import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getByAbbrev } from "../../Common/Services/StateService";
import { PieChart, Pie, Cell, Legend} from "recharts";

const COLORS = ["#00C49F", "#0088FE"];
  
function StatePageStats(){

    const { stateAbbrev } = useParams();
    const [percentVaccinated, setPercentVaccinated] = useState(0);
    const [vacCount, setVacCount] = useState(0);
    const [stateName, setStateName] = useState();
    const [pop, setPop] = useState(0);
    const [data, setData] = useState([]);

    // Get data by state
    useEffect(() => {
        getByAbbrev(stateAbbrev).then((state) => {
            if(state !== undefined){
                setPercentVaccinated(state.get("percentVaccinated"));
                setVacCount(state.get("vaccineCount"));
                setStateName(state.get("name"));
                setPop(state.get("population"));
            }
        });
    }, [stateAbbrev]);

    // Get data for pie chart from prior data
    useEffect(() => {
        if(vacCount !== 0 && pop !==0){
            setData([
                { name: "Vaccinated", value: vacCount },
                { name: "Not Vaccinated", value: pop - vacCount }
            ]);
        }
    }, [pop, vacCount]);
   
    return (
        <div>
            <h3><u>{stateName} ({stateAbbrev})</u></h3>
            <h4>Percent Vaccinated: {percentVaccinated}%</h4>
            <h4>Vaccine Count: {vacCount}</h4>
            <h4>Population: {pop}</h4>
            <PieChart width={400} height={300}>
                <Pie
                    data={data}
                    cx="50%"
                    cy="50%"
                    labelLine={false}
                    outerRadius={80}
                    fill="#8884d8"
                    dataKey="value"
                >
                    {data.map((entry, index) => (
                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                </Pie>
                <Legend />
            </PieChart>
        </div>
    );
};

export default StatePageStats;