import StatePageStats from "./StatePageStats";
import StateComments from "./StatePageComments";

function StatePage() {
    
    return (
        <div>
            <StatePageStats />
            <hr />
            <h4>Comments:</h4>
            <StateComments />
        </div>
    );
};

export default StatePage;