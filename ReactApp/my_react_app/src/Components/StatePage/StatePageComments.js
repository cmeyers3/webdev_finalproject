import { useEffect, useState } from 'react';
import { getStateComments } from '../../Common/Services/CommentService.js';
import { useParams } from "react-router-dom";
import { getByAbbrev } from "../../Common/Services/StateService";

function StateComments(){
    // Retrieve comments from database on page load
    const [comments, setComments] = useState([]);
    const { stateAbbrev } = useParams();

    useEffect(() => {
        //Through the state abbreviation, get id of state and
        //check for comments for that state
        getByAbbrev(stateAbbrev).then((state) => {
            if(state !== undefined){
                getStateComments(state.id).then((comments) => {
                    setComments(comments);
                });
            }
        });
    }, [stateAbbrev]);

    return (
        comments.map(
            comment => ( 
                <div key={comment.id} id='Comments'>         
                    <h4>{comment.attributes.title}</h4>
                    <h6>Author: {comment.attributes.author}</h6>
                    <h5>{comment.attributes.body}</h5>
                </div>
            )
        )
    );
};

export default StateComments;