import React from 'react';
import { Link } from 'react-router-dom';
//import { getCurrentUser } from '../../Common/Services/AuthService.js';

const Footer = () => {
    
    //User authorization (temp removed feature)
    /*
    var user = getCurrentUser();
    var userPath = '/auth';
    if (user) {
        var firstName = user.attributes.firstName;
        var lastName = user.attributes.lastName;
        userPath = '/user/' + firstName + '/' + lastName;
    }*/

    return (
        <footer>
            <nav>
                <ul>
                    <li><Link to="/">Home</Link></li>
                </ul>
            </nav>
        </footer>
    );

};
//If add back auth: <li><Link to={userPath}>User</Link></li>

export default Footer;