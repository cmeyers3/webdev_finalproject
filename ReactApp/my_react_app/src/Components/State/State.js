// Creates a state object
function StateModule({ children, onChildClick, onChildHover, casecount, vaccinecount }) {
    return (
        <button 
            onClick={onChildClick} 
            onMouseOver={onChildHover}
            casecount={casecount}
            vaccinecount={vaccinecount}
        >
            {children}
        </button>
    );
}

export default StateModule;



