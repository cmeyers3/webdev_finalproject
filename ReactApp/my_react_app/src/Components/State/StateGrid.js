import { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import StateModule from './State.js';
import { getAllStates } from '../../Common/Services/StateService.js';

// Creates grid of number buttons
function StateGrid() {
    const history = useHistory();

    // Handle click on child
    function handleClick(e) {
        /*alert(
            e.target.innerText + " has " + e.target.getAttribute('caseCount') + " cases!"
        );*/
        //On click go to state page
        history.push("/state/" + e.target.innerText);
    }
    
    // Handle hover
    function handleHover(e) {
        //alert("Hover on " + e.target.innerText + "!");
        //document.getElementById("popup").style.display = "block";
    }
    
    // Make a list 1-50
    var nums = [];
    for (var i=1; i <= 50; i++) {
        nums.push(i);
    }
    
    // Get all states
    const [states, setStates] = useState([]);
    useEffect(() => {
        getAllStates().then((states) => {
            setStates(states);
        });
    }, []);

    
    return states.map(
        state => 
        <StateModule 
            key={state.attributes.abbrev}
            onChildClick={handleClick} 
            onChildHover={handleHover}
            casecount={state.attributes.caseCount}
            vaccinecount={state.attributes.vaccineCount}
        >
            {state.attributes.abbrev}
        </StateModule>
    );
}

export default StateGrid;
