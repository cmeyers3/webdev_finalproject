import Parse from "parse";
/* SERVICE FOR PARSE SERVER OPERATIONS */

// CREATE operation - new comment
export const createComment = (Title, Body, Author) => {
  const Comment = Parse.Object.extend("Comment");
  const comment = new Comment();
  // using setter to UPDATE the object
  comment.set("title", Title);
  comment.set("body", Body);
  comment.set("author", Author);
  return comment.save().then((result) => {
    // returns new Comment object
    return result;
  });
};

// READ operation - get all comments in Parse class Comment
export const getAllComments = () => {
  const Comment = Parse.Object.extend("Comment");
  const query = new Parse.Query(Comment);
  return query.find().then((results) => {
    // returns array of Comment objects
    return results;
  });
};

// READ operation - get all comments that are connected to a certain state 
export const getStateComments = (id) => {
  const State = Parse.Object.extend("State");
  const query = new Parse.Query(State);
  return query.get(id).then((state) => {
    const Comment = Parse.Object.extend("Comment");
    return new Parse.Query(Comment)
    .include("statePtr").equalTo("statePtr", state).find()
    .then((results) => {
      // returns array of Comment objects
      return results;
    });
  });
};

// DELETE operation - remove comment by ID
export const removeComment = (id) => {
  const Comment = Parse.Object.extend("Comment");
  const query = new Parse.Query(Comment);
  return query.get(id).then((comment) => {
    comment.destroy();
  });
};