import Parse from 'parse';

export const createUser = (newUser) => {
    const user = new Parse.User();

    user.set('username', newUser.email);
    user.set('firstName', newUser.firstName);
    user.set('lastName', newUser.lastName);
    user.set('password', newUser.password);
    user.set('email', newUser.email);

    console.log('User: ', user);
    return user
        .signUp()
        .then((newUserSaved) => {
            return newUserSaved;
        })
        .catch((error) => {
            alert(`Error: ${error.message}`);
        });
};

export const getCurrentUser = () => {
    return Parse.User.current();  
};

export const checkUser = ( username, password ) => {
    console.log("New Username (in checkuser): " + username);
    const user = new Parse.User();
    user.set('username', username);
    user.set('password', password);
    
    console.log('User: ', user);
    
    return user
        .logIn(username, password)
        .catch((error) => {
            alert(`User does not exist with username + password: ${error.message}`);
        });
};

export const isAuthorized = () => {
    if (!Parse.User.current()) {
        return false;
    }
    console.log(Parse.User.current());
    return Parse.User.current().authenticated();  
};

export const logOut = () => {
    Parse.User.logOut();  
};
