import Parse from "parse";
/* SERVICE FOR PARSE SERVER OPERATIONS */

// READ operation - get state by id
export const getById = (id) => {
  const State = Parse.Object.extend("State");
  const query = new Parse.Query(State);
  return query.get(id).then((state) => {
    // return State object with id
    return state;
  });
};

// READ operation - get state by abbreviation
export const getByAbbrev = (abbrev) => {
  const State = Parse.Object.extend("State");
  const query = new Parse.Query(State);
  //BREAKING
  query.equalTo('abbrev', abbrev);
  return query.first();
};

// READ operation - get all states in Parse class State
export const getAllStates = () => {
  const State = Parse.Object.extend("State");
  const query = new Parse.Query(State);
  return query.find().then((states) => {
    // returns array of State objects
    return states;
  });
};

// Get state object by name
export const getStateByName = (name) => {
    // Get all states
    return getAllStates().then((states) => {
        return states.filter((state) => {
            return state.attributes.name === name;
        })
    });
};
