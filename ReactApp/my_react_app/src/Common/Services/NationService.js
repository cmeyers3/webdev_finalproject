import Parse from "parse";
/* SERVICE FOR PARSE SERVER OPERATIONS */

// READ operation - get nation by id
export const getById = (id) => {
  const Nation = Parse.Object.extend("Nation");
  const query = new Parse.Query(Nation);
  return query.get(id).then((nation) => {
    // return Nation object with id
    return nation;
  });
};

// READ operation - get all nations in Parse class Nation
export const getAllNations = () => {
  const Nation = Parse.Object.extend("Nation");
  const query = new Parse.Query(Nation);
  return query.find().then((nations) => {
    // returns array of Nation objects
    return nations;
  });
};
