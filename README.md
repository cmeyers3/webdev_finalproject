# webdev_finalproject

TODO:
- 1. (Feature) Create choropleth map
- 2. (Feature) Create different state pages, each with state info
- 3. (Feature) Add a US map using maybe react-simple-maps
- 4. (Feature) Add hover info boxes
- 5. (Feature) Add comments to state pages
- 6. (Feature) Add search box for navigation of states
- 7. (Want) Style the page (CSS) + organize layout

Ashley: 2 5 6
Charles: 1 3 4

Stories from Charles
1. Choropleth map is added, with a gradient from yellow -> green
3. Added a US map, a prerequisite for 1.
4. Added hover info boxes, mentioning state name, vaccine count, and percent vaccinated
