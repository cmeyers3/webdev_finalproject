[Version 0.2.0]
- Transitioned to Webpack
- Integrated Parse
- Added Components, including Nation, State, User, Comment
- Removed some warnings + errors

[Version 0.3.0]
- Added user accounts
- Register/Login Added
- Protected routes so that users alone can access user pages
- Blocked users from being able to register/login once already logged in.
